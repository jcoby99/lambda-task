## Module 10: Serverless basics

### Sub-task 1 – create a "Weekly CSV Report" Lambda

**1. Create a Lambda function(source code is located in `index.mjs` file)**

![1](screenshots/1.png)

**2. Create an AWS IAM Policy to Grant AWS Lambda Access to an Amazon DynamoDB Table**

![2.1](screenshots/2.1.png)

![2.2](screenshots/2.2.png)

**3.Use FullAccessS3 policy, created in Module 3: IAM to Access S3 from lambda**

![3](screenshots/3.png)

**4.Publish lambda**

![4](screenshots/4.png)

**5. Trigger Lambda manually and ensure csv report is uploaded to S3 bucket**

![5.1](screenshots/5.1.png)

![5.2](screenshots/5.2.png)

![5.3](screenshots/5.3.png)
